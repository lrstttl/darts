import DartsDetector as dd
import time
from threading import Thread
from Tkinter import *
from thread import *
from Classes import *
import socket
import cv2

TWO_WEBCAMS = False

class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        master.minsize(width=800, height=600)
        self.pack()

def printin(event):
    test = str(eval(GUI.e1.get()))
    print test

def calibrateGUI():
    det1.calibrate()
    if TWO_WEBCAMS:
        det2.calibrate()

#det1 = dd.DartsDetector(mount='left')
det1 = dd.DartsDetector(mount='left', path='videos/testvideo.avi')
for i in range(0,120):
    img1 = det1.camera.take_image()    
#cv2.imshow('test', img1)  
if TWO_WEBCAMS:
    det2 = dd.DartsDetector(mount='left', path='videos/testvideo.avi')
    for i in range(0,120):
        img2 = det2.camera.take_image()    
else:
    det2 = det1

winName = "Picture"
finalScore = 0
curr_player = 1
scoreplayer1 = 501
scoreplayer2 = 501

points = []
root = Tk()
GUI = GUIDef()
player = Player()


def GameOn():
    scoreplayer1 = 501
    scoreplayer2 = 501
    global curr_player
    curr_player = 1

    GUI.e1.configure(bg='light green')

    global finalScore
    finalScore = 0
    GUI.e1.delete(0,'end')
    GUI.e2.delete(0,'end')
    GUI.e1.insert(10,scoreplayer1)
    GUI.e2.insert(10,scoreplayer2)
    GUI.finalentry.delete(0, 'end')
    GUI.dart1entry.delete(0, 'end')
    GUI.dart2entry.delete(0, 'end')
    GUI.dart3entry.delete(0, 'end')

    player.player = curr_player
    player.score = 501
    
#    cv2.startWindowThread()
    # start getDart thread!!!
    t = Thread(target=getDarts, args=(player, GUI))
    t.start()


def getDarts(playerObj, GUI):

    # count iterations of processing
    i = 0
    
    finalScore = 0
    count = 0
    breaker = 0
    ## threshold important -> make accessible
    minThres = 4000 # 1000
    maxThres = 10000 # 7500

    # save score if score is below 1...
    old_score = playerObj.score
        
    # Read first image twice (issue somewhere) to start loop:
    _ = det1.camera.take_image()
    if TWO_WEBCAMS:
        _ = det2.camera.take_image()
    # wait for camera
    time.sleep(0.1)
    t_L = det1.camera.take_gray_image()
    if TWO_WEBCAMS:
        t_R = det2.camera.take_gray_image()
    else:
        t_R = t_L.copy()

    while 1:
        
        i += 1
        if i % 10 == 0:
            print "still running..."
            
        # wait for camera
#        time.sleep(0.1)
        # check if dart hit the board (picture is taken here)
        thresh_L, current_L = det1.getThreshold(t_L)
        thresh_R, current_R = det2.getThreshold(t_R)
        
        cv2.imshow("Test", current_L)

        cnz_L = cv2.countNonZero(thresh_L)
        cnz_R = cv2.countNonZero(thresh_R)
        
        ## threshold important
        if (cnz_L > minThres and cnz_L < maxThres) \
            or (cnz_R > minThres and cnz_R < maxThres):
            # wait for camera vibrations
#            time.sleep(0.2)
            # filter noise
            t_plus_L, blur_L = det1.diff2blur(t_L)
            t_plus_R, blur_R = det2.diff2blur(t_R)
            # get corners
            corners_L = det1.getCorners(blur_L)
            corners_R = det2.getCorners(blur_R)

            testimg = blur_L.copy()

            # dart outside?
            if corners_L.size < 40 and corners_R.size < 40:
                print "### dart not detected"
                continue

            # filter corners
            corners_f_L = det1.filterCorners(corners_L)
            corners_f_R = det2.filterCorners(corners_R)

            # dart outside?
            if corners_f_L.size < 30 and corners_f_R.size < 30:
                print "### dart not detected"
                continue

            # find left and rightmost corners#
            rows, cols = blur_L.shape[:2]
            corners_final_L = det1.filterCornersLine(corners_f_L, rows, cols)
            corners_final_R = det2.filterCornersLine(corners_f_R, rows, cols)

            _, thresh_L = cv2.threshold(blur_L, 60, 255, 0)
            _, thresh_R = cv2.threshold(blur_R, 60, 255, 0)

            # check if it was really a dart
            if cv2.countNonZero(thresh_L) > maxThres*2 or cv2.countNonZero(thresh_R) > maxThres*2:
                print "### dart not detected"
                continue

            print "Dart detected"
            # dart was found -> increase counter
            breaker += 1

            dartInfo = DartDef()

            # get final darts location
            try:
                dartInfo_L = DartDef()
                dartInfo_R = DartDef()

                dartInfo_L.corners = corners_final_L.size
                dartInfo_R.corners = corners_final_R.size

                locationofdart_L = det1.getRealLocation(corners_final_L, "left")
                locationofdart_R = det2.getRealLocation(corners_final_R, "right")
                                
                cv2.circle(current_L, (int(locationofdart_L.item(0)), int(locationofdart_L.item(1))), 2, (255, 255, 0), 2, 4)
                cv2.imshow(winName, current_L)

                # check for the location of the dart with the calibration
                dartloc_L = det1.getTransformedLocation(locationofdart_L.item(0), locationofdart_L.item(1))
                dartloc_R = det2.getTransformedLocation(locationofdart_R.item(0), locationofdart_R.item(1))
                                
                # detect region and score
                dartInfo_L = det1.getDartRegion(dartloc_L)
                dartInfo_R = det2.getDartRegion(dartloc_R)
                
                cv2.circle(testimg, (locationofdart_L.item(0), locationofdart_L.item(1)), 2, (0, 255, 0), 2, 8)
                cv2.circle(testimg, (locationofdart_L.item(0), locationofdart_L.item(1)), 10, (255, 255, 255), 2, 8)
            except:
                print "Something went wrong in finding the darts location!"
                breaker -= 1
                continue

            # "merge" scores
            dartInfo = dartInfo_L
            if dartInfo_R.base == dartInfo_L.base and dartInfo_R.multiplier == dartInfo_L.multiplier:
                dartInfo = dartInfo_R
            # use the score of the image with more corners
            else:
                if dartInfo_R.corners > dartInfo_L.corners:
                    dartInfo = dartInfo_R
                else:
                    dartInfo = dartInfo_L

            print "Feld: " + str(dartInfo.base) + "; Multiplikator: " + str(dartInfo.multiplier)

            if breaker == 1:
                GUI.dart1entry.insert(10,str(dartInfo.base * dartInfo.multiplier))
#                cv2.imwrite("frame2.jpg", testimg)     # save dart1 frame
            elif breaker == 2:
                GUI.dart2entry.insert(10,str(dartInfo.base * dartInfo.multiplier))
#                cv2.imwrite("frame3.jpg", testimg)     # save dart2 frame
            elif breaker == 3:
                GUI.dart3entry.insert(10,str(dartInfo.base * dartInfo.multiplier))
#                cv2.imwrite("frame4.jpg", testimg)     # save dart3 frame

            dart = dartInfo.base * dartInfo.multiplier
            playerObj.score -= dart

            if playerObj.score == 0 and dartInfo.multiplier == 2:
                playerObj.score = 0
                breaker = 3
            elif playerObj.score <= 1:
                playerObj.score = old_score
                breaker = 3

            # save new diff img for next dart
            t_L = t_plus_L
            t_R = t_plus_R

            if playerObj.player == 1:          
                GUI.e1.delete(0,'end')
                GUI.e1.insert(10,playerObj.score)
            else:
                GUI.e2.delete(0,'end')
                GUI.e2.insert(10,playerObj.score)

            finalScore += (dartInfo.base * dartInfo.multiplier)

            if breaker == 3:
                break

        # missed dart
        elif cv2.countNonZero(thresh_L) < maxThres/2 or cv2.countNonZero(thresh_R) < maxThres/2:
            continue

        # if player enters zone - break loop
        elif cv2.countNonZero(thresh_L) > maxThres/2 or cv2.countNonZero(thresh_R) > maxThres/2:
            break

#        key = cv2.waitKey(10)
#        key = cv2.waitKey()
#        if key == 27:
#            cv2.destroyWindow(winName)
#            break

        count += 1

    GUI.finalentry.delete(0, 'end')
    GUI.finalentry.insert(10,finalScore)

    print "Final score: " + str(finalScore) 
    print "GetDarts thread exited"


# correct dart score with binding -> press return to change
def dartcorr(event):
    # check if empty, on error write 0 to value of dart
    try:
        dart1 = int(eval(GUI.dart1entry.get()))
    except:
        dart1 = 0
    try:
        dart2 = int(eval(GUI.dart2entry.get()))
    except:
        dart2 = 0
    try:
        dart3 = int(eval(GUI.dart3entry.get()))
    except:
        dart3 = 0

    dartscore = dart1 + dart2 + dart3

    # check which player
    if curr_player == 1:
        new_score = scoreplayer1 - dartscore
        GUI.e1.delete(0,'end')
        GUI.e1.insert(10, new_score)
    else:
        new_score = scoreplayer2 - dartscore
        GUI.e2.delete(0,'end')
        GUI.e2.insert(10, new_score)
    GUI.finalentry.delete(0,'end')
    GUI.finalentry.insert(10,dartscore)
  

# start motion processing in different thread, init scores
def dartscores():
    global scoreplayer1
    global scoreplayer2
    global curr_player
    if curr_player == 1:
        curr_player = 2
        GUI.e2.configure(bg='light green')
        GUI.e1.configure(bg='white')
        score = int(GUI.e2.get())
        player.player = curr_player
        player.score = score
    else:
        curr_player = 1
        GUI.e1.configure(bg='light green')
        GUI.e2.configure(bg='white')
        score = int(GUI.e1.get())
        player.player = curr_player
        player.score = score

    # clear dartscores
    GUI.finalentry.delete(0, 'end')
    GUI.dart1entry.delete(0, 'end')
    GUI.dart2entry.delete(0, 'end')
    GUI.dart3entry.delete(0, 'end')
    scoreplayer1 = int(GUI.e1.get())
    scoreplayer2 = int(GUI.e2.get())

    # start getDart thread!!!
    t = Thread(target=getDarts, args=(player, GUI))
    t.start()


# Background Image
back_gnd = Canvas(root)
back_gnd.pack(expand=True, fill='both')

back_gnd_image = PhotoImage(file="Dartboard.gif")
back_gnd.create_image(0, 0, anchor='nw', image=back_gnd_image)

# Create Buttons
ImagCalib = Button(None, text="Calibrate", fg="black", font = "Helvetica 26 bold", command=calibrateGUI)
back_gnd.create_window(20,200, window=ImagCalib, anchor='nw')

newgame = Button(None, text="Game On!", fg="black", font = "Helvetica 26 bold", command=GameOn)
back_gnd.create_window(20,20, window=newgame, anchor='nw')

QUIT = Button(None, text="QUIT", fg="black", font = "Helvetica 26 bold", command=root.quit)
back_gnd.create_window(20,300, window=QUIT, anchor='nw')

nextplayer = Button(None, text="Next Player", fg="black", font = "Helvetica 26 bold", command=dartscores)
back_gnd.create_window(460,400, window=nextplayer, anchor='nw')


# player labels and entry for total score

#player1 = Label(None, text="Player 1", font = "Helvetica 32 bold")
player1 = Entry(root, font = "Helvetica 32 bold",width=7)
back_gnd.create_window(250,20, window=player1, anchor='nw')
player1.insert(10,"Player 1")

#player2 = Label(None, text="Player 2", font = "Helvetica 32 bold")
player2 = Entry(root, font = "Helvetica 32 bold",width=7)
back_gnd.create_window(400,20, window=player2, anchor='nw')
player2.insert(10,"Player 2")

GUI.e1 = Entry(root,font = "Helvetica 44 bold",width=4)
GUI.e1.bind("<Return>", printin)
back_gnd.create_window(250,80, window=GUI.e1, anchor='nw')
GUI.e2 = Entry(root,font = "Helvetica 44 bold",width=4)
back_gnd.create_window(400,80, window=GUI.e2, anchor='nw')
GUI.e1.insert(10,"501")
GUI.e2.insert(10,"501")

#e1.pack()

# dart throw scores
dart1label = Label(None, text="1.: ", font = "Helvetica 20 bold")
back_gnd.create_window(300,160, window=dart1label, anchor='nw')

GUI.dart1entry = Entry(root,font = "Helvetica 20 bold",width=3)
GUI.dart1entry.bind("<Return>", dartcorr)
back_gnd.create_window(350,160, window=GUI.dart1entry, anchor='nw')

dart2label = Label(None, text="2.: ", font = "Helvetica 20 bold")
back_gnd.create_window(300,210, window=dart2label, anchor='nw')

GUI.dart2entry = Entry(root,font = "Helvetica 20 bold",width=3)
GUI.dart2entry.bind("<Return>", dartcorr)
back_gnd.create_window(350,210, window=GUI.dart2entry, anchor='nw')

dart3label = Label(None, text="3.: ", font = "Helvetica 20 bold")
back_gnd.create_window(300,260, window=dart3label, anchor='nw')

GUI.dart3entry = Entry(root,font = "Helvetica 20 bold",width=3)
GUI.dart3entry.bind("<Return>", dartcorr)
back_gnd.create_window(350,260, window=GUI.dart3entry, anchor='nw')

finallabel = Label(None, text=" = ", font = "Helvetica 20 bold")
back_gnd.create_window(300,310, window=finallabel, anchor='nw')

GUI.finalentry = Entry(root,font = "Helvetica 20 bold",width=3)
back_gnd.create_window(350,310, window=GUI.finalentry, anchor='nw')

app = Application(master=root)
app.mainloop()
root.destroy()