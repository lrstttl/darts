import cv2
import cv2.cv as cv

class Webcam():

    def __init__(self, camera):
        self.camera = camera
        self.cap 	= cv2.VideoCapture(self.camera)	
        self.set_res(960,720)

        """ Supported resolutions for Live! Cam Sync HD:
            640x480 (default), 960x720, 1280x720 """
    def set_res(self, x,y):
        self.cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, int(x))
        self.cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, int(y))
        
    def take_image(self):
        return self.cap.read()[1]	

    def take_gray_image(self):
        image = self.take_image()
        return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)  

    def save_image(self, filename):
        img = self.take_image()
        cv2.imwrite("Camera" + str(self.camera) + "_" + filename + ".jpg", img)

    def read_image(self, path):		
        return cv2.imread(path)

    def show(self):	
        img = self.take_image()
        cv2.imshow('Webcam ' + str(self.camera), img)