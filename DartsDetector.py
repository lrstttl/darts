import numpy as np
import cv2
import cv2.cv as cv
import os.path
import pickle
from shapely.geometry.polygon import LinearRing
from shapely.geometry import LineString

from MathFunctions import *
import Draw as DrawBoard
import Webcam
from Classes import *

def nothing(x):
    pass

class DartsDetector():

    def __init__(self, mount='left', path=[]):
        if path:
            self.camera = Webcam.Webcam(path)
            self.mount = mount
        else:
            if mount == 'left':
                self.camera = Webcam.Webcam(1)
                self.mount = mount
            else:
                self.camera = Webcam.Webcam(2)
                self.mount = 'right'
        self.calData = CalibrationData()
        self.calibrationComplete = False
        self.ref_img = self.camera.take_image()
        
    
    def update_ref(self):
        self.ref_img = self.camera.take_image()
        
    
    def update_img(self):
        self.current_img = self.camera.take_image()
        
        
    def calibrate(self):
        
        imCal = self.ref_img.copy()
        cv2.imwrite("frame1_" + self.mount + ".jpg", imCal)  # save calibration frame
        
        while self.calibrationComplete == False:
            print "-------------------------------------------------------"
            #Read calibration file, if exists
            if os.path.isfile("calibrationData_" + self.mount + ".pkl"):
                try:
                    calFile = open("calibrationData_" + self.mount + ".pkl", 'rb')
                    self.calData = CalibrationData()
                    self.calData = pickle.load(calFile)
                    calFile.close()

                    #copy image for old calibration data                    
                    transformed_img = cv2.warpPerspective(imCal, self.calData.transformation_matrix, (800, 800))
                    
                    draw = DrawBoard.Draw()
                    
                    transformed_img = draw.drawBoard(transformed_img, self.calData)
                    
                    cv2.imshow("Camera", transformed_img)
                    
                    print "Check if old calibration still is functional."
                    print "Press Enter to accept, ESC to start new calibration."        
                    print ""
                    
                    while 1:	
                        # accept old calibration if enter is pressed, exit if ESC is pressed"				
                        key = cv2.waitKey(1)     
                        if key == 13 or key == 32:
                            cv2.destroyAllWindows()
                            #we are good with the previous calibration data
                            self.calibrationComplete = True
                            print "The old calibration files are being used."
                            print "-------------------------------------------------------"
                            return
                        elif key == 27:
                            cv2.destroyAllWindows()
                            #delete the calibration file and start over
                            print "Delete old calibration files and start new calibration."
                            os.remove("calibrationData_" + self.mount + ".pkl")
                            break
        
                #corrupted file
                except EOFError as err:
                    print err

            # start calibration if no calibration data exists
            else:
                print "New calibration started."
                print "Move sliders so that the mask fits the image."        
                print "Press Enter or ESC to accept the calibration."
                
                self.calData = CalibrationData()
                
                self.calData.points = self.getTransformationPoints(imCal)
                # 13/6: 0 | 6/10: 1 | 10/15: 2 | 15/2: 3 | 2/17: 4 | 17/3: 5 | 3/19: 6 | 19/7: 7 | 7/16: 8 | 16/8: 9 |
                # 8/11: 10 | 11/14: 11 | 14/9: 12 | 9/12: 13 | 12/5: 14 | 5/20: 15 | 20/1: 16 | 1/18: 17 | 18/4: 18 | 4/13: 19
                # top, bottom, left, right
                # 12/9, 2/15, 8/16, 13/4
                #calData.dstpoints = [12, 2, 8, 18]
                self.calData.dstpoints = [18, 8, 12, 2]
                self.calData.transformation_matrix = self.manipulateTransformationPoints(imCal)
        
                cv2.destroyAllWindows()
        
                print "The dartboard image has now been calibrated."
                print "-------------------------------------------------------"
                print ""
              
                cv2.imshow("Calibration Image", imCal)
        
                #write the calibration data to a file
                calFile = open("calibrationData_" + self.mount + ".pkl", "wb")
                pickle.dump(self.calData, calFile, 0)
                calFile.close()
        
                self.calibrationComplete = True
                break
            
        print "Already calibrated!"


    def manipulateTransformationPoints(self, imCal):

        cv2.namedWindow('image', cv2.WINDOW_NORMAL)

        cv2.createTrackbar('tx1', 'image', 0, 20, nothing)
        cv2.createTrackbar('ty1', 'image', 0, 20, nothing)
        cv2.createTrackbar('tx2', 'image', 0, 20, nothing)
        cv2.createTrackbar('ty2', 'image', 0, 20, nothing)
        cv2.createTrackbar('tx3', 'image', 0, 20, nothing)
        cv2.createTrackbar('ty3', 'image', 0, 20, nothing)
        cv2.createTrackbar('tx4', 'image', 0, 20, nothing)
        cv2.createTrackbar('ty4', 'image', 0, 20, nothing)
        cv2.setTrackbarPos('tx1', 'image', 10)
        cv2.setTrackbarPos('ty1', 'image', 10)
        cv2.setTrackbarPos('tx2', 'image', 10)
        cv2.setTrackbarPos('ty2', 'image', 10)
        cv2.setTrackbarPos('tx3', 'image', 10)
        cv2.setTrackbarPos('ty3', 'image', 10)
        cv2.setTrackbarPos('tx4', 'image', 10)
        cv2.setTrackbarPos('ty4', 'image', 10)
        # create switch for ON/OFF functionality
        switch = '0 : OFF \n1 : ON'
        cv2.createTrackbar(switch, 'image', 1, 1, nothing)
        imCal_copy = imCal.copy()
        while (1):
            cv2.imshow('image', imCal_copy)
            k = cv2.waitKey(1) & 0xFF
            if k == 27 or k == 13 or k == 32:
                break
            # get current positions of four trackbars
            tx1 = cv2.getTrackbarPos('tx1', 'image') - 10
            ty1 = cv2.getTrackbarPos('ty1', 'image') - 10
            tx2 = cv2.getTrackbarPos('tx2', 'image') - 10
            ty2 = cv2.getTrackbarPos('ty2', 'image') - 10
            tx3 = cv2.getTrackbarPos('tx3', 'image') - 10
            ty3 = cv2.getTrackbarPos('ty3', 'image') - 10
            tx4 = cv2.getTrackbarPos('tx4', 'image') - 10
            ty4 = cv2.getTrackbarPos('ty4', 'image') - 10
            s = cv2.getTrackbarPos(switch, 'image')
            if s == 0:
                imCal_copy[:] = 0
            else:
                # transform the image to form a perfect circle
                transformation_matrix = self.transformation(imCal, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4)
                
        return transformation_matrix

    
    def destinationPoint(self, i):
        dstpoint = [(self.calData.center_dartboard[0] + self.calData.ring_radius[5] * math.cos((0.5 + i) * self.calData.sectorangle)),
                    (self.calData.center_dartboard[1] + self.calData.ring_radius[5] * math.sin((0.5 + i) * self.calData.sectorangle))]
        
        return dstpoint

    
    def transformation(self, imCalRGB, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4):

        points = self.calData.points
        
        ## sectors are sometimes different -> make accessible
        # used when line rectangle intersection at specific segment is used for transformation:
        newtop = self.destinationPoint(self.calData.dstpoints[0])
        newbottom = self.destinationPoint(self.calData.dstpoints[1])
        newleft = self.destinationPoint(self.calData.dstpoints[2])
        newright = self.destinationPoint(self.calData.dstpoints[3])

        # get a fresh new image
        new_image = imCalRGB.copy()

        # create transformation matrix
        src = np.array([(points[0][0]+tx1, points[0][1]+ty1), (points[1][0]+tx2, points[1][1]+ty2),
                        (points[2][0]+tx3, points[2][1]+ty3), (points[3][0]+tx4, points[3][1]+ty4)], np.float32)
        dst = np.array([newtop, newbottom, newleft, newright], np.float32)
        transformation_matrix = cv2.getPerspectiveTransform(src, dst)

        new_image = cv2.warpPerspective(new_image, transformation_matrix, (800, 800))

        # draw image
        drawBoard = DrawBoard.Draw()
        new_image = drawBoard.drawBoard(new_image, self.calData)

        cv2.circle(new_image, (int(newtop[0]), int(newtop[1])), 2, (255, 255, 0), 2, 4)
        cv2.circle(new_image, (int(newbottom[0]), int(newbottom[1])), 2, (255, 255, 0), 2, 4)
        cv2.circle(new_image, (int(newleft[0]), int(newleft[1])), 2, (255, 255, 0), 2, 4)
        cv2.circle(new_image, (int(newright[0]), int(newright[1])), 2, (255, 255, 0), 2, 4)

        cv2.imshow('manipulation', new_image)

        return transformation_matrix


    def findSectorLines(self, edged, Ellipse, image_proc_img):
        lines_seg = []
        verticalFound = 0
        horizontalFound = 0
        
        # fit line to find intersec point for dartboard center point
        lines = cv2.HoughLines(edged, 0.5, np.pi / 150, 30, 100)
          
        ## sector angles important -> make accessible
        for rho, theta in lines[0]:
            
            thetaDeg = np.rad2deg(theta)
            
            if (((thetaDeg > 35) and (thetaDeg < 55) or (thetaDeg > 125) and (thetaDeg < 145))
                and (not horizontalFound)):
                # horizontal line
                
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 2000 * (-b))
                y1 = int(y0 + 2000 * (a))
                x2 = int(x0 - 2000 * (-b))
                y2 = int(y0 - 2000 * (a))     
                
                cv2.line(image_proc_img, (x1, y1), (x2, y2), (255, 0, 0), 1)
                lines_seg.append([(x1, y1), (x2, y2)])
                horizontalFound = 1
            
            if (thetaDeg > 118) and (thetaDeg < 120) and (not verticalFound):
                # vertical line   
                
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 2000 * (-b))
                y1 = int(y0 + 2000 * (a))
                x2 = int(x0 - 2000 * (-b))
                y2 = int(y0 - 2000 * (a)) 
                
                cv2.line(image_proc_img, (x1, y1), (x2, y2), (255, 0, 0), 1)
                lines_seg.append([(x1, y1), (x2, y2)])
                verticalFound = 1
     
        return lines_seg, image_proc_img


    def findEllipseByColor(self, image_proc_img):
        
        window = 'Ellipse Detection'
        cv2.namedWindow(window, cv2.WINDOW_NORMAL)
        
        # color format: (B,G,R)
#        lowerGreen = (20,50,0)
#        upperGreen = (110,120,110)
#        lowerRed = (30,15,75)
#        upperRed = (110,95,215)
        
        cv2.createTrackbar('Green_R_low', window, 0, 80, nothing)
        cv2.createTrackbar('Green_G_low', window, 0, 80, nothing)
        cv2.createTrackbar('Green_B_low', window, 0, 80, nothing)
        cv2.createTrackbar('Green_R_high', window, 80, 150, nothing)
        cv2.createTrackbar('Green_G_high', window, 100, 254, nothing)
        cv2.createTrackbar('Green_B_high', window, 100, 254, nothing)
        cv2.createTrackbar('Red_R_low', window, 0, 100, nothing)
        cv2.createTrackbar('Red_G_low', window, 0, 50, nothing)
        cv2.createTrackbar('Red_B_low', window, 0, 50, nothing)
        cv2.createTrackbar('Red_R_high', window, 150, 254, nothing)
        cv2.createTrackbar('Red_G_high', window, 50, 150, nothing)
        cv2.createTrackbar('Red_B_high', window, 50, 150, nothing)
        cv2.createTrackbar('MinArea', window, 100000/4, 1000000/4, nothing)
        cv2.createTrackbar('MaxArea', window, 700000/4, 1000000/4, nothing)
        
        cv2.setTrackbarPos('Green_R_low', window, 0)
        cv2.setTrackbarPos('Green_G_low', window, 50)
        cv2.setTrackbarPos('Green_B_low', window, 20)
        cv2.setTrackbarPos('Green_R_high', window, 110)
        cv2.setTrackbarPos('Green_G_high', window, 120)
        cv2.setTrackbarPos('Green_B_high', window, 110)
        cv2.setTrackbarPos('Red_R_low', window, 0)
        cv2.setTrackbarPos('Red_G_low', window, 0)
        cv2.setTrackbarPos('Red_B_low', window, 19)
        cv2.setTrackbarPos('Red_R_high', window, 254)
        cv2.setTrackbarPos('Red_G_high', window, 150)
        cv2.setTrackbarPos('Red_B_high', window, 110)
        cv2.setTrackbarPos('MinArea', window, 600000/4)
        cv2.setTrackbarPos('MaxArea', window, 1000000/4)
                
        Ellipse = EllipseDef()
            
        while (1):
            image = image_proc_img.copy()
            # get current positions of four trackbars
            Green_R_low = cv2.getTrackbarPos('Green_R_low', window)
            Green_G_low = cv2.getTrackbarPos('Green_G_low', window)
            Green_B_low = cv2.getTrackbarPos('Green_B_low', window)
            Green_R_high = cv2.getTrackbarPos('Green_R_high', window)
            Green_G_high = cv2.getTrackbarPos('Green_G_high', window)
            Green_B_high = cv2.getTrackbarPos('Green_B_high', window)
            Red_R_low = cv2.getTrackbarPos('Red_R_low', window)
            Red_G_low = cv2.getTrackbarPos('Red_G_low', window)
            Red_B_low = cv2.getTrackbarPos('Red_B_low', window)
            Red_R_high = cv2.getTrackbarPos('Red_R_high', window)
            Red_G_high = cv2.getTrackbarPos('Red_G_high', window)
            Red_B_high = cv2.getTrackbarPos('Red_B_high', window)
            MinArea = cv2.getTrackbarPos('MinArea', window)
            MaxArea = cv2.getTrackbarPos('MaxArea', window)
            
            lowerGreen = (Green_B_low, Green_G_low, Green_R_low)
            upperGreen = (Green_B_high, Green_G_high, Green_R_high)
            lowerRed = (Red_B_low, Red_G_low, Red_R_low)
            upperRed = (Red_B_high, Red_G_high, Red_R_high)
        
            # find the colors within the specified boundaries
            maskGreen = cv2.inRange(image, lowerGreen, upperGreen)
            maskRed   = cv2.inRange(image, lowerRed, upperRed)
                
            ## kernel size important -> make accessible
            # very important -> removes lines outside the outer ellipse -> find ellipse
            kernel = np.ones((3, 3), np.uint8)
        
            maskRed = cv2.morphologyEx(maskRed, cv2.MORPH_CLOSE, kernel, iterations=6)
            maskRed = cv2.morphologyEx(maskRed, cv2.MORPH_OPEN, kernel, iterations=1) 
        
            maskGreen = cv2.morphologyEx(maskGreen, cv2.MORPH_CLOSE, kernel, iterations=3)  
            maskGreen = cv2.morphologyEx(maskGreen, cv2.MORPH_OPEN, kernel, iterations=1)  
        
            mask = cv2.bitwise_or(maskGreen, maskRed)

            mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel, iterations=1)  
            mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel, iterations=6)        
        
#            cv2.imshow("Masked Red", maskRed)
#            cv2.imshow("Masked Green", maskGreen)
#            cv2.imshow("Mask", mask)         
        
            # find contours in thresholded image, then grab only large ones
            contours, hierarchy = cv2.findContours(mask, 1, 2)
        
            found = 0
        
            if len(contours) != 0:
                ## contourArea threshold important -> make accessible
                for cnt in contours:
                    try:  # threshold critical, change on demand?
                        if MinArea < cv2.contourArea(cnt) < MaxArea:
                            found = 1
                            ellipse = cv2.fitEllipse(cnt)
                            cv2.ellipse(image, ellipse, (0, 255, 0), 2)
    
                            x, y = ellipse[0]
                            a, b = ellipse[1]
                            angle = ellipse[2]
    
                            a = a / 2
                            b = b / 2
    
                            cv2.ellipse(image, (int(x), int(y)), (int(a), int(b)), int(angle), 0.0, 360.0,
                                        (255, 0, 0))
                            
#                            cv2.imshow("Image", image)
    
                    # corrupted file
                    except:
                        print "error"
#                        return Ellipse, image_proc_img
                        
                if found:
                    Ellipse.a = a
                    Ellipse.b = b
                    Ellipse.x = x
                    Ellipse.y = y
                    Ellipse.angle = angle
#                    return Ellipse, image_proc_img
            else:
                print "no contours found, change RGB values"
#                return Ellipse, image_proc_img
            
            cv2.imshow("Image", image)
            
            k = cv2.waitKey(1) & 0xFF
            if k == 27 or k == 13 or k == 32:
                break
                
        cv2.destroyWindow(window)
        return Ellipse, image_proc_img
                        

    def getTransformationPoints(self, image_proc_img):
        imCalHSV = cv2.cvtColor(image_proc_img, cv2.COLOR_BGR2HSV)
        kernel = np.ones((5, 5), np.float32) / 25
        blur = cv2.filter2D(imCalHSV, -1, kernel)
        h, s, imCal = cv2.split(blur)
        
        ## threshold important -> make accessible
        ret, thresh = cv2.threshold(imCal, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        ## kernel size important -> make accessible
        # very important -> removes lines outside the outer ellipse -> find ellipse
        kernel = np.ones((5, 5), np.uint8)
        thresh2 = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        #cv2.imshow("thresh2", thresh2)

        # find enclosing ellipse
        Ellipse, image_proc_img = self.findEllipseByColor(image_proc_img)

        # calculate edged image
        edged = cv2.Canny(thresh2, 250, 255)
        #cv2.imshow("Edged", edged)        

        # find 2 sector lines -> horizontal and vertical sector line 
        lines_seg, image_proc_img = self.findSectorLines(edged, Ellipse, image_proc_img)
        
        # find intersection points -> source points for transformation
        # points are automatically ordered: -> top, bottom, left, right 
        source_points = self.getEllipseLineIntersection(Ellipse, lines_seg)  
        
        cv2.circle(image_proc_img, (int(source_points[0][0]), int(source_points[0][1])), 3, (255, 0, 0), 2, 8)
        cv2.circle(image_proc_img, (int(source_points[1][0]), int(source_points[1][1])), 3, (255, 0, 0), 2, 8)
        cv2.circle(image_proc_img, (int(source_points[2][0]), int(source_points[2][1])), 3, (255, 0, 0), 2, 8)
        cv2.circle(image_proc_img, (int(source_points[3][0]), int(source_points[3][1])), 3, (255, 0, 0), 2, 8)
                
        #cv2.imshow("Ellipses + Lines", image_proc_img)
        
        return source_points
                   
    
    def getEllipseLineIntersection(self, Ellipse, lines_seg):
        
        intersectp_s = []
        a = self.ellipse_polyline(Ellipse)
        ea = LinearRing(a)
        
        for lin in lines_seg:
            line_p1 = lin[0]     
            line_p2 = lin[1]             
            line = LineString([line_p1,line_p2])
            
            mp = ea.intersection(line)   
            
            if mp.is_empty:
                print('Geometries do not intersect')
            elif mp.geom_type == 'Point':
                intersectp_s.append([mp.x, mp.y])
            elif mp.geom_type == 'MultiPoint':
                for p in mp:
                    intersectp_s.append([p.x, p.y])        
            else:
                raise ValueError('something unexpected: ' + mp.geom_type)
            
        #now order intersection points
        source_points = self.orderIntersectp_s(intersectp_s)
        
        return source_points
    

    def ellipse_polyline(self, Ellipse, n=100):
        t = np.linspace(0, 2*np.pi, n, endpoint=False)
        st = np.sin(t)
        ct = np.cos(t)    
        
        angle = np.deg2rad(Ellipse.angle)
        sa = np.sin(angle)
        ca = np.cos(angle)
        p = np.empty((n, 2))
        p[:, 0] = Ellipse.x + Ellipse.a * ca * ct - Ellipse.b * sa * st
        p[:, 1] = Ellipse.y + Ellipse.a * sa * ct + Ellipse.b * ca * st
        
        return p    
    
    
    def orderIntersectp_s(self, intersectp_s):
            
        source_points = []
        print intersectp_s
        pointarray = np.array(intersectp_s)
        top_idx = [np.argmin(pointarray[:, 1])][0]
        bot_idx = [np.argmax(pointarray[:, 1])][0]
        top = intersectp_s[top_idx]    
        bot = intersectp_s[bot_idx]
        intersectp_s.remove(top)
        intersectp_s.remove(bot)    
        
        if self.mount == "left":
            right_idx = [np.argmax(pointarray[:, 0])][0]
            left_idx = [np.argmin(pointarray[:, 0])][0]
        else:
            right_idx = [np.argmin(pointarray[:, 0])][0]
            left_idx = [np.argmax(pointarray[:, 0])][0]
                
#        top = intersectp_s[top_idx]    
        left = intersectp_s[left_idx] 
#        intersectp_s.remove(top)
        intersectp_s.remove(left)
#        intersectp_s.remove(bot)        
        right = intersectp_s[0]
            
        source_points.append(top)  # top
        source_points.append(bot)  # bottom
        source_points.append(left)  # left
        source_points.append(right)  # right
        
        return source_points
        
        
########################### Recognition methods ###############################
    def getThreshold(self, t):
        t_plus = self.camera.take_gray_image()       
        dimg = cv2.absdiff(t, t_plus)
    
        blur = cv2.GaussianBlur(dimg, (5, 5), 0)
        blur = cv2.bilateralFilter(blur, 9, 75, 75)
        _, thresh = cv2.threshold(blur, 60, 255, 0)
    
        return thresh, t_plus
    
    
    def diff2blur(self, t):
        t_plus = self.camera.take_gray_image() 
        dimg = cv2.absdiff(t, t_plus)
    
        ## kernel size important -> make accessible
        # filter noise from image distortions
        kernel = np.ones((5, 5), np.float32) / 25
        blur = cv2.filter2D(dimg, -1, kernel)
    
        return t_plus, blur        
        
        
    def getCorners(self, img_in):
        # number of features to track is a distinctive feature
        ## FeaturesToTrack important -> make accessible
        edges = cv2.goodFeaturesToTrack(img_in, 640, 0.0008, 1, mask=None, blockSize=3, useHarrisDetector=1, k=0.06)  # k=0.08
        corners = np.int0(edges)
    
        return corners
        
    def filterCorners(self, corners):
        cornerdata = []
        tt = 0
        mean_corners = np.mean(corners, axis=0)
        for i in corners:
            xl, yl = i.ravel()
            # filter noise to only get dart arrow
            ## threshold important -> make accessible
            if abs(mean_corners[0][0] - xl) > 180:
                cornerdata.append(tt)
            if abs(mean_corners[0][1] - yl) > 120:
                cornerdata.append(tt)
            tt += 1
    
        corners_new = np.delete(corners, [cornerdata], axis=0)  # delete corners to form new array
    
        return corners_new
    
    
    def filterCornersLine(self, corners, rows, cols):
        [vx, vy, x, y] = cv2.fitLine(corners, cv.CV_DIST_HUBER, 0, 0.1, 0.1)
        lefty = int((-x * vy / vx) + y)
        righty = int(((cols - x) * vy / vx) + y)
    
        cornerdata = []
        tt = 0
        for i in corners:
            xl, yl = i.ravel()
            # check distance to fitted line, only draw corners within certain range
            distance = dist(0, lefty, cols - 1, righty, xl, yl)
            if distance > 40:  ## threshold important -> make accessible
                cornerdata.append(tt)
    
            tt += 1
    
        corners_final = np.delete(corners, [cornerdata], axis=0)  # delete corners to form new array
    
        return corners_final        
        
        
    def getRealLocation(self, corners_final, mount):
    
        if self.mount == "right":
            loc = np.argmax(corners_final, axis=0)
        else:
            loc = np.argmin(corners_final, axis=0)
    
        locationofdart = corners_final[loc]
    
        # check if dart location has neighbouring corners (if not -> continue)
        cornerdata = []
        tt = 0
        for i in corners_final:
            xl, yl = i.ravel()
            distance = abs(locationofdart.item(0) - xl) + abs(locationofdart.item(1) - yl)
            if distance < 40:  ## threshold important
                tt += 1
            else:
                cornerdata.append(tt)
    
        if tt < 3:
            corners_temp = cornerdata
            maxloc = np.argmax(corners_temp, axis=0)
            locationofdart = corners_temp[maxloc]
            print "### used different location due to noise!"
    
        return locationofdart
        
        
        
        
        
###################### (x,y) to points mapping methods ########################
    def getTransformedLocation(self, x_coord,y_coord):
        try:
            # transform only the hit point with the saved transformation matrix
            # ToDo: idea for second camera -> transform complete image and overlap both images to find dart location?
            dart_loc_temp = np.array([[x_coord, y_coord]], dtype="float32")
            dart_loc_temp = np.array([dart_loc_temp])
            
            dart_loc = cv2.perspectiveTransform(dart_loc_temp, self.calData.transformation_matrix)
            
            new_dart_loc = tuple(dart_loc.reshape(1, -1)[0])
            
            return new_dart_loc
    
        #system not calibrated
        except AttributeError as err1:
            print err1
            return (-1, -1)
    
        except NameError as err2:
            #not calibrated error
            print err2
            return (-2, -2)        
        
        
    #Returns dartThrow (score, multiplier, angle, magnitude) based on x,y location
    def getDartRegion(self, dart_loc):
        try:
            height = 800
            width = 800
                
            dartInfo = DartDef()
    
            #find the magnitude and angle of the dart
            vx = (dart_loc[0] - width/2)
            vy = (height/2 - dart_loc[1])
            
            # reference angle for atan2 conversion
            ref_angle = 81
    
            dartInfo.magnitude = math.sqrt(math.pow(vx, 2) + math.pow(vy, 2))
            dartInfo.angle = math.fmod(((math.atan2(vy,vx) * 180/math.pi) + 360 - ref_angle), 360)
                
            angleDiffMul = int((dartInfo.angle) / 18.0)
                
            #starting from the 20 points
            if angleDiffMul == 0:
                dartInfo.base = 20
            elif angleDiffMul == 1:
                dartInfo.base = 5
            elif angleDiffMul == 2:
                dartInfo.base = 12
            elif angleDiffMul == 3:
                dartInfo.base = 9
            elif angleDiffMul == 4:
                dartInfo.base = 14
            elif angleDiffMul == 5:
                dartInfo.base = 11
            elif angleDiffMul == 6:
                dartInfo.base = 8
            elif angleDiffMul == 7:
                dartInfo.base = 16
            elif angleDiffMul == 8:
                dartInfo.base = 7
            elif angleDiffMul == 9:
                dartInfo.base = 19
            elif angleDiffMul == 10:
                dartInfo.base = 3
            elif angleDiffMul == 11:
                dartInfo.base = 17
            elif angleDiffMul == 12:
                dartInfo.base = 2
            elif angleDiffMul == 13:
                dartInfo.base = 15
            elif angleDiffMul == 14:
                dartInfo.base = 10
            elif angleDiffMul == 15:
                dartInfo.base = 6
            elif angleDiffMul == 16:
                dartInfo.base = 13
            elif angleDiffMul == 17:
                dartInfo.base = 4
            elif angleDiffMul == 18:
                dartInfo.base = 18
            elif angleDiffMul == 19:
                dartInfo.base = 1
            else:
                #something went wrong
                dartInfo.base = -300
                
            #Calculating multiplier (and special cases for Bull's Eye):
            for i in range(0, len(self.calData.ring_radius)):
                #Find the ring that encloses the dart
                if dartInfo.magnitude <= self.calData.ring_radius[i]:
                    #Bull's eye, adjust base score
                    if i == 0:
                        dartInfo.base = 25
                        dartInfo.multiplier = 2
                    elif i == 1:
                        dartInfo.base = 25
                        dartInfo.multiplier = 1
                    #triple ring
                    elif i == 3:
                        dartInfo.multiplier = 3
                    #double ring
                    elif i == 5:
                        dartInfo.multiplier = 2
                    #single
                    elif i == 2 or i == 4:
                        dartInfo.multiplier = 1
                    #finished calculation
                    break
    
            #miss
            if dartInfo.magnitude > self.calData.ring_radius[5]:
                dartInfo.base = 0
                dartInfo.multiplier = 0
                
            return dartInfo
    
    
        #system not calibrated
        except AttributeError as err1:
            print "AttributeError: "+ err1
            dartInfo = DartDef()
            return dartInfo
    
        except NameError as err2:
            #not calibrated error
            print "NameError: " + err2
            dartInfo = DartDef()
            return dartInfo
        