import Webcam
import cv2

def main():
	cam1 = Webcam.Webcam(1)
	i = 0
	
	print "-------------------------"
	print "Press Enter to take image"
	print "Press ESC to exit"
	print "-------------------------"
		
	while 1:	
		# take and save image if enter is pressed, exit if ESC is pressed"				
		key = cv2.waitKey(1)
		cam1.show()
		
		if key == 13:
			cam1.save_image(str(i))
			print "Image " + str(i) + " saved"
			i = i+1
		elif key == 27:
			break			
			
			
	del cam1
	cv2.destroyAllWindows()
	

if __name__ == '__main__':
	main()
