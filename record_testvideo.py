# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 14:18:43 2018

@author: Lars
"""

import numpy as np
import cv2
import cv2.cv as cv
import time

cap = cv2.VideoCapture(1)
cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, int(960))
cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, int(720))

w = cap.get(cv.CV_CAP_PROP_FRAME_WIDTH);
h = cap.get(cv.CV_CAP_PROP_FRAME_HEIGHT); 
fourcc = cv.CV_FOURCC('I','4','2','0')
out = cv2.VideoWriter('videos/testvideo.avi',fourcc, 15.0, (int(w),int(h)))


i = 0

while (cap.isOpened()):
    i += 1
    ret, frame = cap.read()
    if ret == True:
        if i > 10:
            out.write(frame)
        cv2.imshow('frame', frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
    else:
        break
    
cap.release()
out.release()
cv2.destroyAllWindows()